//
//  Settings.swift
//  isen
//
//  Created by Reshad Farid on 02-06-16.
//  Copyright © 2016 Panda Pixl. All rights reserved.
//

import Foundation

public class Settings {
    
    public static let prefs:NSUserDefaults = NSUserDefaults()
    
    public enum SettingsError: ErrorType {
        case Empty
        case Invalid
        case Custom(String)
    }
    
    public class var sharedInstance: Settings {
        struct Singleton {
            static let instance = Settings()
        }
        return Singleton.instance
    }
    
    public class func todayFilter(val: Bool) {
        
        prefs.setBool(val, forKey: "today")
        
    }
    
    public class func isTodayFilter() -> Bool {
        
        guard (prefs.objectForKey("today") != nil) else {
            return false
        }
        
        return prefs.objectForKey("today") as! Bool
    }
}
