//
//  DeviceListViewController.swift
//  isen
//
//  Created by Reshad Farid on 26-05-16.
//  Copyright © 2016 Panda Pixl. All rights reserved.
//

import UIKit
import CoreBluetooth
import Foundation

protocol DeviceListViewControllerDelegate {
    var connectionMode:ConnectionMode { get }
    func connectPeripheral(peripheral:CBPeripheral, mode:ConnectionMode)
    func stopScan()
    func startScan()
    func setReference(aDelegate: DeviceListViewController)
}

class DeviceListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // must have properties for devicelist
    var delegate: DeviceListViewControllerDelegate?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var helpButton: UIBarButtonItem!
    var devices:[BLEDevice] = []
    private var tableIsLoading = false
    private var signalImages:[UIImage]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = BLEManager.sharedInstance
        delegate?.setReference(self)
        delegate?.startScan()
        print("Delegation: \(self.delegate)") // output is nil ?!
        self.signalImages = [UIImage](arrayLiteral: UIImage(named: "signalStrength-0.png")!,
                                      UIImage(named: "signalStrength-1.png")!,
                                      UIImage(named: "signalStrength-2.png")!,
                                      UIImage(named: "signalStrength-3.png")!,
                                      UIImage(named: "signalStrength-4.png")!)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        
        //Add pull-to-refresh functionality
        let tvc = UITableViewController(style: UITableViewStyle.Plain)
        tvc.tableView = tableView
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(DeviceListViewController.refreshWasPulled(_:)), forControlEvents: UIControlEvents.ValueChanged)
        tvc.refreshControl = refresh
        
    }
    
    func connectInMode(mode:ConnectionMode, peripheral:CBPeripheral) {
        
        switch mode {
        case ConnectionMode.UART,
             ConnectionMode.PinIO,
             ConnectionMode.Info,
             ConnectionMode.Controller:
            delegate?.connectPeripheral(peripheral, mode: mode)
        default:
            break
        }
        
    }
    
    func didFindPeripheral(peripheral:CBPeripheral!, advertisementData:[NSObject : AnyObject]!, RSSI:NSNumber!) {
        
        //        println("\(self.classForCoder.description()) didFindPeripheral")
        
        //If device is already listed, just update RSSI
        let newID = peripheral.identifier
        for device in devices {
            if device.identifier == newID {
                //                println("   \(self.classForCoder.description()) updating device RSSI")
                device.RSSI = RSSI
                return
            }
        }
        
        //Add reference to new device
        let newDevice = BLEDevice(peripheral: peripheral, advertisementData: advertisementData, RSSI: RSSI)
        newDevice.printAdData()
        devices.append(newDevice)
        
        //Reload tableview to show new device
        if tableView != nil {
            tableIsLoading = true
            tableView.reloadData()
            self.navigationItem.title = "apparaten gevonden"
            tableIsLoading = false
        }
    }
    
    func refreshWasPulled(sender:UIRefreshControl) {
        
        delegate?.stopScan()
        
        devices.removeAll()
        
        delay(0.45, closure: { () -> () in
            sender.endRefreshing()
            
            delay(0.25, closure: { () -> () in
                self.tableIsLoading = true
                self.tableView.reloadData()
                self.tableIsLoading = false
                self.navigationItem.title = "Geen apparaten gevonden"
                self.delegate?.startScan()
            })
        })
    }
    
    @IBAction func dismissViewController(sender: UIBarButtonItem) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func clearDevices() {
        
        delegate?.stopScan()
        
        devices.removeAll()
        
        
        tableIsLoading = true
        tableView.reloadData()
        tableIsLoading = false
        delegate?.startScan()
        
        self.navigationItem.title = "Geen apparaten gevonden"
    }
    
    
    //MARK: TableView functions
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("deviceCell", forIndexPath: indexPath) as! DeviceCell

        cell.nameLabel.text = devices[indexPath.row].name
        cell.subtitleLabel.text = devices[indexPath.row].isUART ? "Dit is mogelijk het apparaat wat u zoekt" : "Dit zoekt u waarschijnlijk niet"
        cell.signalImages = signalImages
        cell.updateSignalImage(devices[indexPath.row].RSSI)
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devices.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Bluetooth apparaten"
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        printLog(self, funcName: "connectButtonTapped", logString: "")
        
        let device = devices[indexPath.row]
        
        if (device.isUART) {
            self.connectInMode(ConnectionMode.UART, peripheral: device.peripheral)
        }
        
        dispatch_async(dispatch_get_main_queue(), {
            self.tableView.reloadData()
        })
        
    }
}
