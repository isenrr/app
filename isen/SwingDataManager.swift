//
//  SwingDataManager.swift
//  isen
//
//  Created by Reshad Farid on 22-06-16.
//  Copyright © 2016 Panda Pixl. All rights reserved.
//

import RealmSwift
import CoreLocation

class SwingDataManager {
    
    internal class var sharedInstance: SwingDataManager {
        struct Singleton {
            static let instance = SwingDataManager()
        }
        return Singleton.instance
    }
    
    var notificationToken: NotificationToken?
    let realm = try! Realm()
    
    init() {
        
        notificationToken = self.realm.addNotificationBlock { [unowned self] note, realm in
            NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: "realmDidUpdate", object: nil))
        }
        
    }
    
    func getAllObjects() -> Results<Swing> {
        
        let swings = self.realm.objects(Swing).sorted("date")
        
        return swings
        
    }
    
    func getObject(identifier: String) -> Results<Swing> {
        
        let predicate = NSPredicate(format: "identifier = %@", identifier)
        let swing = self.realm.objects(Swing.self).filter(predicate)
        
        return swing
        
    }
    
    func storeObject(location: CLLocation, speed: Int) {
        
        let latitude = location.coordinate.latitude
        let longitude = location.coordinate.longitude
        
        let swing = Swing()
        swing.latitude = latitude
        swing.longitude = longitude
        swing.speed = speed
        
        try! self.realm.write {
            self.realm.add(swing)
        }
        
    }
    
    func deleteObject(identifier: String) {
        
        let swing = realm.objectForPrimaryKey(Swing.self, key: identifier)
        
        try! self.realm.write {
            self.realm.delete(swing!)
        }
        
    }
    
}
