//
//  SwingViewController.swift
//  isen
//
//  Created by Reshad Farid on 16-06-16.
//  Copyright © 2016 Panda Pixl. All rights reserved.
//

import UIKit
import RealmSwift

class SwingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var notificationToken: NotificationToken?
    var results = SwingDataManager.sharedInstance.getAllObjects()
    var dataSource: [[Swing]]!
    
    @IBOutlet weak var filterView: UIButton!
    @IBOutlet weak var mapView: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    var tableIsLoading = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SwingViewController.updateTableView), name:"realmDidUpdate", object: nil)

        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        
        //Add pull-to-refresh functionality
        let tvc = UITableViewController(style: UITableViewStyle.Plain)
        tvc.tableView = tableView
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(SwingViewController.refreshWasPulled(_:)), forControlEvents: UIControlEvents.ValueChanged)
        tvc.refreshControl = refresh
        
        if results.count > 0 {
            updateTableView()
        }
    }
    
    func updateTableView() {
        self.updateSections()
        self.tableView.reloadData()
    }
    
    func updateSections() {
        
        // Formate the NSDate property to how I want it to look at my section titles:
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MM yyyy"
        
        var lastDateString = dateFormatter.stringFromDate((results.first?.date)!).uppercaseString

        var section = [Swing]()
        var dataSource = [[Swing]]()
        
        for i in 0..<results.count {
            let currentDateString = dateFormatter.stringFromDate(results[i].date).uppercaseString
            if lastDateString != currentDateString {
                lastDateString = currentDateString
                dataSource.append(section)
                section = [results[i]]
            } else {
                section.append(results[i])
            }
        }
       dataSource.append(section)
       self.dataSource = dataSource
        
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let currentSection = dataSource[section]
        return currentSection.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return dataSource != nil ? dataSource.count : 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("swingCell", forIndexPath: indexPath) as! SwingCell
        
        let currentSection = dataSource[indexPath.section]
        let currentSwing = currentSection[indexPath.row]
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale.currentLocale()
        dateFormatter.dateFormat = "HH:mm"
        let newTime = dateFormatter.stringFromDate(currentSwing.date)
        
        cell.detail.text = newTime
        cell.title.text = "\(currentSwing.speed) km/h"
        
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            
            let currentSection = dataSource[indexPath.section]
            let currentSwing = currentSection[indexPath.row]
            
            SwingDataManager.sharedInstance.deleteObject(currentSwing.id)
            self.tableView.reloadData()
            
        }
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let currentSection = dataSource[section]
        let firstSwing = currentSection.first
        
        // Format and set the section's title as the section title for the first book in each section:
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let sectionTitle = dateFormatter.stringFromDate((firstSwing?.date)!).uppercaseString
        
        return sectionTitle
    }
    
    func refreshWasPulled(sender:UIRefreshControl) {

        
        delay(0.45, closure: { () -> () in
            sender.endRefreshing()
            
            delay(0.25, closure: { () -> () in
                self.tableIsLoading = true
                self.tableView.reloadData()
                self.tableIsLoading = false
            })
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
