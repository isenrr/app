//
//  BLEManagerController.swift
//  isen
//
//  Created by Reshad Farid on 14-06-16.
//  Copyright © 2016 Panda Pixl. All rights reserved.
//

import Foundation
import CoreBluetooth
import CoreLocation
import UIKit

enum ConnectionMode:Int {
    case None
    case PinIO
    case UART
    case Info
    case Controller
    case DFU
}

protocol BLEManagerDelegate : Any {
    func onDeviceConnectionChange(peripheral:CBPeripheral)
}

class BLEManager: NSObject, CBCentralManagerDelegate,
BLEPeripheralDelegate, UARTViewControllerDelegate, DeviceListViewControllerDelegate, SettingsTableViewControllerDelegate, CLLocationManagerDelegate{
    
    enum ConnectionStatus:Int {
        case Idle = 0
        case Scanning
        case Connected
        case Connecting
    }
    
    var connectionMode:ConnectionMode = ConnectionMode.None
    var connectionStatus:ConnectionStatus = ConnectionStatus.Idle
    var uartViewController:UARTViewController!
    var deviceListViewController:DeviceListViewController!
    var settingsTableViewController: SettingsTableViewController!
    
    private var cm:CBCentralManager?
    private var currentAlertView:UIAlertController?
    private var currentPeripheral:BLEPeripheral?
    private let cbcmQueue = dispatch_queue_create("com.adafruit.bluefruitconnect.cbcmqueue", DISPATCH_QUEUE_CONCURRENT)
    private let connectionTimeOutIntvl:NSTimeInterval = 30.0
    private var connectionTimer:NSTimer?
    
    private let backgroundQueue : dispatch_queue_t = dispatch_queue_create("com.adafruit.bluefruitconnect.bgqueue", nil)
    private var consoleAsciiText:NSAttributedString? = NSAttributedString(string: "")
    private var consoleHexText: NSAttributedString? = NSAttributedString(string: "")
    
    var delegate: BLEManagerDelegate?
    
    let locationManager = CLLocationManager()
    
    internal class var sharedInstance: BLEManager {
        struct Singleton {
            static let instance = BLEManager()
        }
        return Singleton.instance
    }
    
    func centralManager()->CBCentralManager{
        
        return cm!;
        
    }
    
    override init() {
        super.init()
        print("BLE Manager launched")
        // Create core bluetooth manager on launch
        if (cm == nil) {
            cm = CBCentralManager(delegate: self, queue: cbcmQueue)
            
            connectionMode = ConnectionMode.None
            connectionStatus = ConnectionStatus.Idle
            currentAlertView = nil
        }
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.requestCurrentUserLocation()
        
    }
    
    required init(coder aDecoder: NSCoder) {
        
    }
    
    func setReference(aDelegate: DeviceListViewController) {
        self.deviceListViewController = aDelegate
    }
    
    func setReferenceToUART(aDelegate: UARTViewController) {
        self.uartViewController = aDelegate
    }
    
    func setReferenceToSettings(aDelegate: SettingsTableViewController) {
        self.settingsTableViewController = aDelegate
    }
    
    
    //for Objective-C delegate compatibility
    func setDelegate(newDelegate:AnyObject){
        
        if newDelegate.respondsToSelector(Selector("onDeviceConnectionChange:")){
            delegate = newDelegate as? BLEManagerDelegate
        }
        else {
            printLog(self, funcName: "setDelegate", logString: "failed to set delegate")
        }
        
    }
    
    
    func didBecomeActive() {
        
        // Application returned from background state
        
        // Adjust warning label
        if cm?.state == CBCentralManagerState.PoweredOff {
            
            // ble disabled
            
        }
        else if deviceListViewController.devices.count == 0 {
            
        }
        else {

        }
        
    }
    
    func stopScan(){
        
        if (connectionMode == ConnectionMode.None) {
            cm?.stopScan()
            
            connectionStatus = ConnectionStatus.Idle
        }
        
        
        //        else if (connectionMode == ConnectionMode.UART) {
        //
        //        }
        
    }
    
    
    func startScan() {
        //Check if Bluetooth is enabled
        if cm?.state == CBCentralManagerState.PoweredOff {
            print("bluetooth disabled")
            return
        }
        print("scanning for peripherals")
        cm!.scanForPeripheralsWithServices(nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
    }
    
    
    func connectPeripheral(peripheral:CBPeripheral, mode:ConnectionMode) {
        
        //Check if Bluetooth is enabled
        if cm?.state == CBCentralManagerState.PoweredOff {
            print("bluetooth disabled")
            return
        }
        
        printLog(self, funcName: "connectPeripheral", logString: "")
        
        connectionTimer?.invalidate()
        
        if cm == nil {
            //            println(self.description)
            printLog(self, funcName: (#function), logString: "No central Manager found, unable to connect peripheral")
            return
        }
        
        stopScan()
        
        
        //Cancel any current or pending connection to the peripheral
        if peripheral.state == CBPeripheralState.Connected || peripheral.state == CBPeripheralState.Connecting {
            cm!.cancelPeripheralConnection(peripheral)
        }
        
        //Connect
        currentPeripheral = BLEPeripheral(peripheral: peripheral, delegate: self)
        cm!.connectPeripheral(peripheral, options: [CBConnectPeripheralOptionNotifyOnDisconnectionKey: NSNumber(bool:true)])
        
        connectionMode = mode
        connectionStatus = ConnectionStatus.Connecting
        
        // Start connection timeout timer
        connectionTimer = NSTimer.scheduledTimerWithTimeInterval(connectionTimeOutIntvl, target: self, selector: #selector(BLEManager.connectionTimedOut(_:)), userInfo: nil, repeats: false)
    }
    
    
    func connectionTimedOut(timer:NSTimer) {
        
        if connectionStatus != ConnectionStatus.Connecting {
            return
        }
        
        print("Connection status at timeout \(connectionStatus)")
        
        //Cancel current connection
        abortConnection()
        
        printLog(self, funcName: "connectionTimedOut", logString: "Connection timed out")
        
    }
    
    
    func abortConnection() {
        
        connectionTimer?.invalidate()
        
        if (cm != nil) && (currentPeripheral != nil) {
            cm!.cancelPeripheralConnection(currentPeripheral!.currentPeripheral)
        }
        
        currentPeripheral = nil
        
        connectionMode = ConnectionMode.None
        connectionStatus = ConnectionStatus.Idle
    }
    
    func disconnect() {
        
        printLog(self, funcName: (#function), logString: "")
        
        if cm == nil {
            printLog(self, funcName: (#function), logString: "No central Manager found, unable to disconnect peripheral")
            return
        }
            
        else if currentPeripheral == nil {
            printLog(self, funcName: (#function), logString: "No current peripheral found, unable to disconnect peripheral")
            return
        }
        
        //Cancel any current or pending connection to the peripheral
        let peripheral = currentPeripheral!.currentPeripheral
        if peripheral.state == CBPeripheralState.Connected || peripheral.state == CBPeripheralState.Connecting {
            cm!.cancelPeripheralConnection(peripheral)
        }
        
        setDisconnectedUI()
        
    }
    
    
    func alertDismissedOnError() {
        
        if (connectionStatus == ConnectionStatus.Connected) {
            disconnect()
        }
        else if (connectionStatus == ConnectionStatus.Scanning){
            
            if cm == nil {
                printLog(self, funcName: "alertView clickedButtonAtIndex", logString: "No central Manager found, unable to stop scan")
                return
            }
            
            stopScan()
        }
        
        connectionStatus = ConnectionStatus.Idle
        connectionMode = ConnectionMode.None
        
        currentAlertView = nil
        
    }
    
    //MARK: CBCentralManagerDelegate methods
    
    func centralManagerDidUpdateState(central: CBCentralManager) {
        
        if (central.state == CBCentralManagerState.PoweredOn){
            
            //respond to powered on
        }
            
        else if (central.state == CBCentralManagerState.PoweredOff){
            
            //respond to powered off
        }
        
    }
    
    
    func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
        
        if connectionMode == ConnectionMode.None {
            dispatch_sync(dispatch_get_main_queue(), { () -> Void in
                if self.deviceListViewController == nil {
                    print("Device list view controller is nil")
                    return
                }
                self.deviceListViewController.didFindPeripheral(peripheral, advertisementData: advertisementData, RSSI:RSSI)
            })
        }
    }
    
    
    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
        
        if (delegate != nil) {
            delegate!.onDeviceConnectionChange(peripheral)
        }
        
        if currentPeripheral == nil {
            printLog(self, funcName: "didConnectPeripheral", logString: "No current peripheral found, unable to connect")
            return
        }
        
        
        if currentPeripheral!.currentPeripheral == peripheral {
            
            printLog(self, funcName: "didConnectPeripheral", logString: "\(peripheral.name)")
            
            //Discover Services for device
            if((peripheral.services) != nil){
                printLog(self, funcName: "didConnectPeripheral", logString: "Did connect to existing peripheral \(peripheral.name)")
                currentPeripheral!.peripheral(peripheral, didDiscoverServices: nil)  //already discovered services, DO NOT re-discover. Just pass along the peripheral.
            }
            else {
                currentPeripheral!.didConnect(connectionMode)
            }
            
        }
    }
    
    
    func centralManager(central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        
        //respond to disconnection
        
        if (delegate != nil) {
            delegate!.onDeviceConnectionChange(peripheral)
        }
        
        printLog(self, funcName: "didDisconnectPeripheral", logString: "")
        
        if currentPeripheral == nil {
            printLog(self, funcName: "didDisconnectPeripheral", logString: "No current peripheral found, unable to disconnect")
            return
        }
        
        //if we were in the process of scanning/connecting, dismiss alert
        if (currentAlertView != nil) {
            uartDidEncounterError("Peripheral disconnected")
        }
        
        //if status was connected, then disconnect was unexpected by the user, show alert
        if  connectionStatus == ConnectionStatus.Connected {
            
            printLog(self, funcName: "centralManager:didDisconnectPeripheral", logString: "unexpected disconnect while connected")
            
            //return to main view
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.respondToUnexpectedDisconnect()
            })
        }
            
            // Disconnected while connecting
        else if connectionStatus == ConnectionStatus.Connecting {
            
            abortConnection()
            
            printLog(self, funcName: "centralManager:didDisconnectPeripheral", logString: "unexpected disconnect while connecting")
            
            //return to main view
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.respondToUnexpectedDisconnect()
            })
            
        }
        
        connectionStatus = ConnectionStatus.Idle
        connectionMode = ConnectionMode.None
        currentPeripheral = nil
        
        // Dereference mode controllers
        dereferenceModeController()
        
    }
    
    
    func centralManager(central: CBCentralManager, didFailToConnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        
        if (delegate != nil) {
            delegate!.onDeviceConnectionChange(peripheral)
        }
        
    }
    
    
    func respondToUnexpectedDisconnect() {
        
        setDisconnectedUI()
        
        //display disconnect alert
        let alert = UIAlertView(title:"Verbinding verbroken",
                                message:"BLE device verbinding verloren",
                                delegate:self,
                                cancelButtonTitle:"OK")
        
        let note = UILocalNotification()
        note.fireDate = NSDate().dateByAddingTimeInterval(0.0)
        note.alertBody = "BLE device disconnected"
        note.soundName =  UILocalNotificationDefaultSoundName
        UIApplication.sharedApplication().scheduleLocalNotification(note)
        
        alert.show()
        
        
    }
    
    func setDisconnectedUI() {
    
        dispatch_async(dispatch_get_main_queue(), {
            if self.settingsTableViewController != nil {
                self.settingsTableViewController.peripheralName.text = "Er is geen sensor verbonden"
                self.settingsTableViewController.peripheralStatus.text = "Niet verbonden"
                self.settingsTableViewController.tableView.reloadData()
                NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: "connectionWithPeripheral", object: nil, userInfo: ["connected":false]))
            }
        })
        
    }
    
    
    func dereferenceModeController() {
        uartViewController = nil
    }
    
    
    //MARK: BLEPeripheralDelegate methods
    
    func connectionFinalized() {
        
        //Bail if we aren't in the process of connecting
        if connectionStatus != ConnectionStatus.Connecting {
            printLog(self, funcName: "connectionFinalized", logString: "with incorrect state")
            return
        }
        
        if (currentPeripheral == nil) {
            printLog(self, funcName: "connectionFinalized", logString: "Unable to start info w nil currentPeripheral")
            return
        }
        
        //stop time out timer
        connectionTimer?.invalidate()
        
        connectionStatus = ConnectionStatus.Connected
        
        dispatch_async(dispatch_get_main_queue(), {
            if self.settingsTableViewController != nil {
                self.settingsTableViewController.peripheralName.text = self.currentPeripheral?.currentPeripheral.name
                self.settingsTableViewController.peripheralStatus.text = "Verbonden"
                NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: "connectionWithPeripheral", object: nil, userInfo: ["connected":true]))

            }
        })
        
        deviceListViewController.dismissViewControllerAnimated(true, completion: nil)
//        let data = "TEST".dataUsingEncoding(NSUTF8StringEncoding)
//        sendData(data!)
    }
    
    
    func uartDidEncounterError(error: NSString) {
        
        printLog(self, funcName: "uartDidEncounterError", logString: error as String)
    }
    
    
    func didReceiveData(newData: NSData) {
        
        //Data incoming from UART peripheral, forward to current view controller
        
        printLog(self, funcName: "didReceiveData", logString: "\(newData.hexRepresentationWithSpaces(true))")
        print("\(connectionStatus)")
        
        if (connectionStatus == ConnectionStatus.Connected ) {
            //UART
            if (connectionMode == ConnectionMode.UART) {
                //send data to UART Controller
                printLog(self, funcName: "didReceiveData", logString: "Received data with connection")
                updateConsoleWithIncomingData(newData)
            }
        }
        else {
            printLog(self, funcName: "didReceiveData", logString: "Received data without connection")
        }
        
    }
    
    var registeredSpeed = 0
    
    func updateConsoleWithIncomingData(newData:NSData) {
        
        //Write new received data to the console text view
        dispatch_async(backgroundQueue, { () -> Void in
            //convert data to string & replace characters we can't display
            let dataLength:Int = newData.length
            var data = [UInt8](count: dataLength, repeatedValue: 0)
            
            newData.getBytes(&data, length: dataLength)
            
            for index in 0...dataLength-1 {
                if (data[index] <= 0x1f) || (data[index] >= 0x80) { //null characters
                    if (data[index] != 0x9)       //0x9 == TAB
                        && (data[index] != 0xa)   //0xA == NL
                        && (data[index] != 0xd) { //0xD == CR
                        data[index] = 0xA9
                    }
                    
                }
            }
            
            
            let newString = NSString(bytes: &data, length: dataLength, encoding: NSUTF8StringEncoding)
            printLog(self, funcName: "updateConsoleWithIncomingData", logString: newString! as String)
            
            //Check for notification command & send if needed
            //            if newString?.containsString(self.notificationCommandString) == true {
            //                printLog(self, "Checking for notification", "does contain match")
            //                let msgString = newString!.stringByReplacingOccurrencesOfString(self.notificationCommandString, withString: "")
            //                self.sendNotification(msgString)
            //            }
            
            // check if we have to save the last registered speed
            if ((newString?.containsString("save")) == true) {
                
                let datamanager = SwingDataManager.sharedInstance
                
                if self.isAvailableLocation {
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        datamanager.storeObject(self.lastLocation!, speed: self.registeredSpeed)
                        print("saved a location")
                    })
                }
            }
            
            //Update ASCII text on background thread A
            let appendString = "" // or "\n"
            let attrAString = NSAttributedString(string: ((newString! as String)+appendString), attributes: nil)
            let newAsciiText = NSMutableAttributedString(attributedString: self.consoleAsciiText!)
            newAsciiText.appendAttributedString(attrAString)
            
            let newHexString = newData.hexRepresentationWithSpaces(true)
            let attrHString = NSAttributedString(string: newHexString as String, attributes: nil)
            let newHexText = NSMutableAttributedString(attributedString: self.consoleHexText!)
            newHexText.appendAttributedString(attrHString)
            
            
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print("\(attrAString.string)")
                
                let array = attrAString.string.split(regex: ",")
                var dataset = [String:AnyObject]()
                if array.count == 1 {
                    dataset = ["m":array[0]]
                } else if array.count == 2 {
                    dataset = ["m":array[1], "s":array[0]]
                } else {
                    dataset = [:]
                }
                
                let speed = dataset.first?.1 as! String
                let formatedMspeed = Double(speed) ?? 0
                self.registeredSpeed = Int(String(format: "%.0f", formatedMspeed))!
                
                NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: "dataRecieving", object: nil, userInfo: dataset))
            })
        })
        
    }
    
    
    func peripheralDidDisconnect() {
        
        //respond to device disconnecting
        
        printLog(self, funcName: "peripheralDidDisconnect", logString: "")
        
        //if we were in the process of scanning/connecting, dismiss alert
        if (currentAlertView != nil) {
            print("view error deallocating")
            uartDidEncounterError("Peripheral disconnected")
        }
        
        //if status was connected, then disconnect was unexpected by the user, show alert
        if  connectionStatus == ConnectionStatus.Connected {
            
            printLog(self, funcName: "peripheralDidDisconnect", logString: "unexpected disconnect while connected")
            
            //return to main view
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.respondToUnexpectedDisconnect()
            })
        }
        
        connectionStatus = ConnectionStatus.Idle
        connectionMode = ConnectionMode.None
        currentPeripheral = nil
        
        // Dereference mode controllers
        dereferenceModeController()
        
    }
    
    //MARK: UartViewControllerDelegate / PinIOViewControllerDelegate methods
    
    func sendData(newData: NSData) {
        
        //Output data to UART peripheral
        
        let hexString = newData.hexRepresentationWithSpaces(true)
        
        printLog(self, funcName: "sendData", logString: "\(hexString)")
        
        
        if currentPeripheral == nil {
            printLog(self, funcName: "sendData", logString: "No current peripheral found, unable to send data")
            return
        }
        
        currentPeripheral!.writeRawData(newData)
        
    }
    
    var lastLocation : CLLocation?
    var isAvailableLocation = false
    
    func requestCurrentUserLocation() {
        self.locationManager.requestLocation()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        switch CLLocationManager.authorizationStatus() {
            case .AuthorizedAlways, .AuthorizedWhenInUse:
                if let location = locations.first {
                    print("Found user's location: \(location)")
                    self.lastLocation = location
                    self.isAvailableLocation = true
                }
        default:
            locationManager.requestAlwaysAuthorization()
        }
        
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
}