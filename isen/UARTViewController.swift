//
//  FirstViewController.swift
//  isen
//
//  Created by Reshad Farid on 26-05-16.
//  Copyright © 2016 Panda Pixl. All rights reserved.
//

import Foundation
import UIKit
import Dispatch

protocol UARTViewControllerDelegate {
    
    func sendData(newData:NSData)
    func setReferenceToUART(aDelegate: UARTViewController)
}

class UARTViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    var delegate:UARTViewControllerDelegate?
    let connectLbl = UILabel()
    let speedLbl = UILabel()
    let maxLbl = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = BLEManager.sharedInstance
        delegate?.setReferenceToUART(self)
        
        notConnected()
        setObservers()
    }
    
    func setObservers() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(UARTViewController.updateConnectionInfo(_:)), name:"connectionWithPeripheral", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(UARTViewController.updateSpeed(_:)), name:"dataRecieving", object: nil)
    }
    
    func updateSpeed(notification: NSNotification) {
        
        var mspeed: String
        var sspeed: String
        
        switch notification.userInfo!.count {
        case 1:
            mspeed = notification.userInfo!["m"] as! String
        case 2:
            mspeed = notification.userInfo!["m"] as! String
            sspeed = notification.userInfo!["s"] as! String
            let formatedSspeed = Double(sspeed) ?? 0
            speedLbl.text = String(format: "%.0f", formatedSspeed)
            let formatedMspeed = Double(mspeed) ?? 0
            maxLbl.text = "Max gemeten: \(String(format: "%.0f", formatedMspeed)) km/h"
        default:
            sspeed = "0"
            mspeed = "0"
            let formatedSspeed = Double(sspeed)
            speedLbl.text = String(format: "%.0f", formatedSspeed!)
            maxLbl.text = "Geen snelheid vastgesteld"
        }
    }
    
    func addSpeed() {
        speedLbl.frame = CGRect(x: self.view.center.x - 100, y: self.view.center.y - 45, width: 200, height: 90)
        speedLbl.text = "0"
        speedLbl.numberOfLines = 0
        speedLbl.textAlignment = NSTextAlignment.Center
        speedLbl.lineBreakMode = NSLineBreakMode.ByWordWrapping
        speedLbl.font = speedLbl.font.fontWithSize(80)
        self.view.addSubview(speedLbl)
        
        maxLbl.frame = CGRect(x: 0, y: 5, width: 400, height: 90)
        maxLbl.text = "Geen snelheid vastgesteld"
        maxLbl.numberOfLines = 0
        maxLbl.textAlignment = NSTextAlignment.Center
        maxLbl.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.view.addSubview(maxLbl)
    }
    
    func removeSpeed() {
        speedLbl.removeFromSuperview()
        maxLbl.removeFromSuperview()
    }

    func updateConnectionInfo(notification: NSNotification) {
        
        let isConnected: Bool = notification.userInfo!["connected"] as! Bool
        
        if isConnected {
            removeNotConnected()
            addSpeed()
        } else {
            notConnected()
            removeSpeed()
        }
    }
    
    func notConnected() {
        
        connectLbl.frame = CGRect(x: self.view.center.x - 100, y: self.view.center.y - 45, width: 200, height: 90)
        connectLbl.text = "Geen sensor verbonden Ga naar instellingen en verbind een sensor"
        connectLbl.numberOfLines = 0
        connectLbl.textAlignment = NSTextAlignment.Center
        connectLbl.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.view.addSubview(connectLbl)

    }
    
    func removeNotConnected() {
        connectLbl.removeFromSuperview()
    }

}

