//
//  Swing.swift
//  isen
//
//  Created by Reshad Farid on 22-06-16.
//  Copyright © 2016 Panda Pixl. All rights reserved.
//

import RealmSwift

class Swing: Object {
    
    dynamic var id : String = NSUUID().UUIDString
    dynamic var latitude : Double = 0.0
    dynamic var longitude : Double = 0.0
    dynamic var speed : Int = 0
    dynamic var date : NSDate = NSDate()
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    var title: String {
        return "Snelheid: \(speed) km/h"
    }
    
    var subtitle: String {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy HH:mm"
        
        let dt = dateFormatter.stringFromDate(date)
        
        return "Datum: \(dt)"
    }
    
    class func speedString() -> String {
        return "title"
    }
    
    class func dateString() -> String {
        return "subtitle"
    }

}
