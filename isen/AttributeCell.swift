//
//  AttributeCell.swift
//  isen
//
//  Created by Reshad Farid on 27-05-16.
//  Copyright © 2016 Panda Pixl. All rights reserved.
//

import UIKit

class AttributeCell: UITableViewCell {
    
    var label:UILabel!
    var button:UIButton!
    var dataStrings:[String]! {
        didSet {
            var string:String = ""
            for i in 0...(dataStrings.count-1) {
                if i == 0 {
                    string = " \(dataStrings[i]):"
                }
                else {
                    string += " \(dataStrings[i])"
                }
            }
            self.label?.text = string
        }
    }
}
