//
//  SecondViewController.swift
//  isen
//
//  Created by Reshad Farid on 26-05-16.
//  Copyright © 2016 Panda Pixl. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import RealmSwift
import RealmMapView

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var mapView: RealmMapView!
    
    
    let locationManager = CLLocationManager()
    var region = MKCoordinateRegion()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
        self.mapView.fetchedResultsController.clusterTitleFormatString = "$OBJECTSCOUNT slagen in dit gebied"
        self.mapView.titleKeyPath = Swing.speedString()
        self.mapView.subtitleKeyPath = Swing.dateString()
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.mapView.showsUserLocation = true
        self.mapView.showsCompass = true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.mapView.refreshMapView()
    }

    @IBAction func zoomOnUserLocationTapped() {
        zoomOnUserLocation()
    }
    
    func zoomOnUserLocation() {
        self.mapView.setRegion(region, animated: true)
    }
    // MARK: Location delegate methods
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        
        region = MKCoordinateRegion(center: center, span: MKCoordinateSpanMake(0.007, 0.007))

    }
    
    
    @IBAction func dismissViewController(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    


}

extension MapViewController {
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        if let safeObjects = ABFClusterAnnotationView.safeObjectsForClusterAnnotationView(view) {
            
            if let firstObjectName = safeObjects.first?.toObject(Swing).speed {
                print("First Object: \(firstObjectName)")
            }
        }
    }
}
