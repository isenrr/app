//
//  SwingCell.swift
//  isen
//
//  Created by Reshad Farid on 22-06-16.
//  Copyright © 2016 Panda Pixl. All rights reserved.
//

import UIKit

class SwingCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var detail: UILabel!
}
