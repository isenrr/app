//
//  DeviceCell.swift
//  isen
//
//  Created by Reshad Farid on 26-05-16.
//  Copyright © 2016 Panda Pixl. All rights reserved.
//

import UIKit

class DeviceCell: UITableViewCell {
    
    var rssiLabel:UILabel!
    var uartCapableLabel:UILabel!
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var signalImageView: UIImageView!
    
    var signalImages:[UIImage]!
    private var lastSigIndex = -1
    private var lastSigUpdate:Double = NSDate.timeIntervalSinceReferenceDate()
    private let updateIntvl = 3.0
    

    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func updateSignalImage(RSSI:NSNumber) {
        
        // Only update every few seconds
        let now = NSDate.timeIntervalSinceReferenceDate()
        if lastSigIndex != -1 && (now - lastSigUpdate) < updateIntvl {
            return
        }
        
        let rssiInt = RSSI.integerValue
        var rssiString = RSSI.stringValue
        var index = 0
        
        if rssiInt == 127 {     // value of 127 reserved for RSSI not available
            index = 0
            rssiString = "N/A"
        }
        else if rssiInt <= -84 {
            index = 0
        }
        else if rssiInt <= -72 {
            index = 1
        }
        else if rssiInt <= -60 {
            index = 2
        }
        else if rssiInt <= -48 {
            index = 3
        }
        else {
            index = 4
        }
        
        if index != lastSigIndex {
            signalImageView.image = signalImages[index]
            lastSigIndex = index
            lastSigUpdate = now
        }
        
    }
    
}
